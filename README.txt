CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Parameter Message module allow create Messages in CMS with parameter. If
this parameter exists in the URL it displays the message.


REQUIREMENTS
------------

No special requirements


INSTALLATION
------------

Install as you would normally install a contributed Drupal module. See:
https://drupal.org/documentation/install/modules-themes/modules-8 for further
information.


CONFIGURATION
-------------

 * Configure your messages in Configuration » User interface » Parameter Message
 
   - Set the parameter and value
   - Set the Message Text.
   - Set the type: (status, warning or error).
   - Format: parameter=value|Message to show|type


MAINTAINERS
-----------

Current maintainers:
 * Renato Gonçalves (RenatoG) - https://www.drupal.org/user/3326031
