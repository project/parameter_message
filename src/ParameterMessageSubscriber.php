<?php

namespace Drupal\parameter_message;

use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\Component\Utility\Xss;

/**
 * Default class for Subscriber.
 */
class ParameterMessageSubscriber implements EventSubscriberInterface {

  /**
   * Check if exists a parameter to show message.
   */
  public function processParameterMessage(GetResponseEvent $event) {

    $config = \Drupal::config('parameter_message.settings');

    $messages = $config->get('messages');

    if (empty($messages)) {
      return FALSE;
    }

    $messages_settings = explode(PHP_EOL, $messages);
    $parameters = \Drupal::request()->query->all();

    foreach ($messages_settings as $message_settings) {

      $message = explode('|', $message_settings);

      $parameter_settings = Xss::filter($message[0]);

      $parameter = trim($parameter_settings);

      $text_message = Xss::filter($message[1]);
      $text_message = trim($text_message);

      $type = Xss::filter($message[2]);
      $type = trim($type);

      $parameter_data = explode('=', $parameter);

      $parameter_key = $parameter_data[0];
      $parameter_value = $parameter_data[1];

      if (!empty($parameters[$parameter_key]) && $parameters[$parameter_key] == $parameter_value) {
        \Drupal::messenger()->addMessage($text_message, $type);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  static public function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = array('processParameterMessage');
    return $events;
  }

}
